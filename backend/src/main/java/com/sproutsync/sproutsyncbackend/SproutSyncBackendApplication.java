package com.sproutsync.sproutsyncbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SproutSyncBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(SproutSyncBackendApplication.class, args);
    }

}
