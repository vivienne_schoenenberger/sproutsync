package com.sproutsync.sproutsyncbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SproutSyncApplication {

	public static void main(String[] args) {
		SpringApplication.run(SproutSyncApplication.class, args);
	}

}
