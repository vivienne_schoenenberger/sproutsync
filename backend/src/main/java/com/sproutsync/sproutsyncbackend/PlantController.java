package com.sproutsync.sproutsyncbackend;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PlantController {
    private final PlantRepository repository;
    public PlantController(PlantRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/plants")
    List<Plant> all(){
        return repository.findAll();
    }
    @PostMapping("/addPlant")
    public ResponseEntity<?> addPlant(@RequestBody Plant plant) {
        try{
            repository.save(plant);
            return ResponseEntity.ok(plant);
        }catch (Exception e){
            return ResponseEntity.status(500).body("Error saving plant: " + e.getMessage());
        }

    }

}
