package com.sproutsync.sproutsyncbackend;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Plant {

    @Id
    Long id;

    String description;

    int fertilization;

    int water;

    String sunlight;
    String thumbnail;




}
