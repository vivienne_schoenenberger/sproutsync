import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YourPlantsComponent } from './your-plants.component';

describe('YourPlantsComponent', () => {
  let component: YourPlantsComponent;
  let fixture: ComponentFixture<YourPlantsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [YourPlantsComponent]
    });
    fixture = TestBed.createComponent(YourPlantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
