import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { PlantListComponent } from './components/plant-list/plant-list.component';
import { YourPlantsComponent } from './your-plants/your-plants.component';
const routes: Routes = [
  // Existing routes (if any)
  { path: 'your-plants', component: YourPlantsComponent },
  // Other routes
];

@NgModule({
  declarations: [
    AppComponent,
    PlantListComponent,
    YourPlantsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes), // Add this line to configure routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
