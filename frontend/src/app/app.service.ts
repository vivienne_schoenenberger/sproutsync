
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
export class MyHttpService {
  constructor(private http: HttpClient){
    this.loadPlants();

  }

  loadPlants(): Observable<any>{
    return  this.http.get('https://perenual.com/api/species-list?page=2&key=sk-dSs364d645d500aa41844&indoor=1')
  }
  postToBackend(data: any): Observable<any> {
    const url = 'http://localhost:8080/addPlant'; 

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.post(url, JSON.stringify(data), httpOptions);
  }
}
