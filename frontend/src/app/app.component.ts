import { Component } from '@angular/core';
import { Router} from '@angular/router';
import { MyHttpService } from './app.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  currentRoute: string = '';
  
  constructor(
    
    private myHttpService: MyHttpService,
    private router: Router
    
    ){
  }
  title = 'angular-app';

  ngOnInit() {
    this.router.events.subscribe(() => {
      this.currentRoute = this.router.url;
      console.log("this is the current url" + this.currentRoute);

      // Perform actions or conditionally render content based on the currentRoute value
    });

    this.myHttpService.loadPlants().subscribe(
      (data) => {
        console.log(data);
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
