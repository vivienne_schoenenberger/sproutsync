import { Component, OnInit } from '@angular/core';
import { MyHttpService } from '../../app.service';
@Component({
  selector: 'app-plant-list',
  templateUrl: './plant-list.component.html',
  styleUrls: ['./plant-list.component.css']
})
export class PlantListComponent {
  plants: any[] = [];
  sortedPlants: any[] = [];
  searchQuery: string = '';
  constructor(private myHttpService: MyHttpService) { }



  ngOnInit() {
    this.myHttpService.loadPlants().subscribe(
      (data) => {
        this.plants = data.data;
        this.sortedPlants = this.plants.slice();
      },
      (error) => {
        console.error(error);
      }
    );

  }
  onSearch(): void {
    this.sortedPlants = this.plants
    .filter(plant =>
      plant.common_name.toLowerCase().includes(this.searchQuery.toLowerCase())
    )
    .slice(); 
  }
  buildPlantObject(plant:any): any {
    // Construct the Plant object here
    const plantData = {
      id: plant.id,
      description: plant.common_name,
      fertilization: 2,
      water: 3,
      sunlight: "plant.sunlight",
      
    };

    // Call sendPostRequest with the constructed object
    this.sendPostRequest(plantData);
  }

  sendPostRequest(plant:any) {

    this.myHttpService.postToBackend(plant).subscribe(
      (response) => {
        console.log('Response from backend:', response);
        // Handle the response as needed
      },
      (error) => {
        console.error('Error:', error);
        console.error('PlantObject:', plant);
      }
    );
  }


  
}
